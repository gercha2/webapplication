<?php
require_once "pdo.php";
session_start();

if (isset($_POST['delete'])  && isset($_POST['profile_id'])){
  $sql = "DELETE FROM profile WHERE profile_id = :zip";
  $stmt = $pdo->prepare($sql);
  $stmt->execute(array(':zip'=> $_POST['profile_id']));
  echo ($sql);
  $_SESSION['success'] = 'Record deleted';
  header('location: index.php');
  return;
}

// Guardian: Make sure that autos_id is present
if ( ! isset($_GET['profile_id']) ) {
  $_SESSION['error'] = "Missing profile_id";
  header('Location: index.php');
  return;
}

$stmt = $pdo->prepare("SELECT first_name, last_name, profile_id FROM profile WHERE profile_id = :xyz");
$stmt->execute(array(":xyz" => $_GET['profile_id']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);
if ($row === false){
  $_SESSION['error'] = 'Bad value for profile_id';
  header('location: index.php');
  return;
}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="description" content="database of automovile">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="style.css">
<title>German Alfredo Chavarro Chavarro</title>
</head>
<body>

<p>Confirm: Deleting <?= htmlentities($row['first_name'].' '.$row['last_name']) ?></p>

<form method="POST">
   <input type="hidden" name="profile_id" value="<?= $row['profile_id'] ?>">
   <input type="submit" name="delete" value="Delete">
   <a href="index.php">Cancel</a>
</form>
</body>
</html>
