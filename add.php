<?php
//Database Communication
require_once "pdo.php";
require_once "util.php";

#checking fisrt if a session exist
session_start();
if ( ! isset($_SESSION['user_id']) ) {
  die("ACCESS DENIED");
}

#handle the incomming data
if (isset($_POST['first_name']) &&
    isset($_POST['last_name']) &&
    isset($_POST['email']) &&
    isset($_POST['headline']) &&
    isset($_POST['summary'])) {
    #validateProfile entries
    $msg = validateProfile();
    if (is_string($msg)){
      $_SESSION['error'] = $msg;
      header('location: add.php');
      return;
    }
    #validate Position entries
    $msg = validatePos();
    if (is_string($msg)){
      $_SESSION['error'] = $msg;
      header('location: add.php');
      return;
    }

    #validate Education entries
    $msg = validateEdu();
    if (is_string($msg)){
      $_SESSION['error'] = $msg;
      header('location: add.php');
      return;
    }

    $sql = "INSERT INTO profile (user_id, first_name, last_name, email, headline, summary) VALUES (:uid, :fn, :lan, :em, :he, :su)";
    echo ("<pre>\n".$sql."\n</pre>\n");
    $stmt = $pdo->prepare($sql);
    $stmt->execute(array(
      ':uid' => $_SESSION['user_id'], //user_id taken from valid id field on server
      ':fn'=> htmlentities($_POST['first_name']),
      ':lan'=> htmlentities($_POST['last_name']),
      ':em' => htmlentities($_POST['email']),
      ':he' => htmlentities($_POST['headline']),
      ':su' => htmlentities($_POST['summary']))
    );
      #Retrieve Id
      $profile_id = $pdo->lastInsertId();
      insertPositions($pdo, $profile_id);
      insertEducations($pdo, $profile_id);

      $_SESSION['success'] = 'Profile added';
      header("location: index.php");
      return;
}

if (isset($_POST['logout'])){
  header('location: logout.php');
  return;
}
///////////////////////////////////////HTML - Visual/////////////////////////////////////
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="description" content="database of automovile">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
  <!--<link rel="stylesheet" href="style.css">-->

<title>German Alfredo Chavarro Chavarro</title>
<script type="text/javascript">

localStorage.i = Number(1);
countPos = 0;

function Education(action) {
    window.console && console.log('Enter to Function()');
    var i = localStorage.i;
    var div = document.createElement('div');

    if (action.id == "add_Edu"){
      if (countPos >= 9){
        alert('Maximum of nine positions entries exceded');
        return;
      }
        countPos++;
        window.console && console.log('Adding Education'+countPos);
        localStorage.i = Number(localStorage.i) + Number(1);
        var id = i;
        div.id = id;
        div.innerHTML = '<br>'+'Year: <input type="text" name="edu_year'+countPos+'" value=""/>' +
        //div.innerHTML = '<br>' + Year: <input type ="text" name = "year" value = "">'+
        '<input type="button" id='+id+' onClick="Education(this); return false" value="-"/>' + '<br><br>' +
        'School: <input type="text" size="80" name="edu_school'+countPos+'" class="school" value="" />';

        document.getElementById('AddDel_Edu').appendChild(div);

      } else {
        countPos--;
        window.console && console.log('Delete Education'+countPos);
        alert('¿Estas seguro que deseas eliminar un campo?');
        var element = document.getElementById(action.id);
        element.parentNode.removeChild(element);
      }
}

localStorage.i = Number(1);
countPos = 0;

function Position(action) {
    window.console && console.log('Enter to Function()');
    var i = localStorage.i;
    var div = document.createElement('div');

    if (action.id == "add"){
      if (countPos >= 9){
        alert('Maximum of nine positions entries exceded');
        return;
      }
        countPos++;
        window.console && console.log('Adding Position'+countPos);
        localStorage.i = Number(localStorage.i) + Number(1);
        var id = i;
        div.id = id;
        div.innerHTML = '<br>'+'Year: <input type="text" name="year'+countPos+'" value=""/>' +
        //div.innerHTML = '<br>' + Year: <input type ="text" name = "year" value = "">'+
        '<input type="button" id='+id+' onClick="Position(this); return false" value="-"/>' + '<br><br>' +
        '<textarea name = "desc'+countPos+'" value="" rows="8" cols="80"/>';

        document.getElementById('AddDel').appendChild(div);

      } else {
        countPos--;
        window.console && console.log('Delete Position'+countPos);
        alert('¿Estas seguro que deseas eliminar un campo?');
        var element = document.getElementById(action.id);
        element.parentNode.removeChild(element);
      }
}
//Autocomplete call
//$('.school').autocomplete({ source: "school.php" });
</script>
</head>

<body><h1>Adding profile for <?php echo ($_SESSION['name'])?></h1>
<?php
if (isset($_REQUEST['name'])){
      echo "<h1><p>Tracking Autos by ";
      echo htmlentities($_REQUEST['name']); //is used "name" value because it is send it by POST method
      echo "</p></h1>\n";
    }
 // line added to turn on color syntax highlight
flashMessages();

?>
<fieldset>
  <form method="POST">
    <p>First Name:<input type="text" name="first_name" value = "" size="80"></p>
    <p>Last Name:<input type="text" name="last_name" value = "" size="80"></p>
    <p>Email:<input type="text" name="email" value = "" size="40"></p>
    <p>Headline:<input type="text" name="headline" value = "" size="40"></p>
    <p>Summary:<input type="text" name="summary" value="" size="40"></p>
    <div id='AddDel_Edu'>
    Education:<input type="submit" id = "add_Edu" onclick="Education(this); return false;" value="+">
    </div>
    <div id='AddDel'>
    Position:<input type="submit" id = "add" onclick="Position(this); return false;" value="+">
    </div>
    <p><input type="submit" name="dopost" value = "Add" size="40">
    <input type="button" onClick="window.location='index.php';" name="logout" value="Cancel"></p>
  </form>
</fieldset>
</body>
</html>
