<?php
//User verification
//Account: umsi@umich.edu
//Password: php123
require_once "pdo.php";
require_once "util.php";
require_once "head.php";

session_start();

#Checking if session is active
if ( ! isset($_SESSION['user_id']) ) {
  die("ACCESS DENIED");
  return;
}
#make sure if profile_id is MysqlndUhPreparedStatement
if (! isset($_REQUEST['profile_id']) ){
  $_SESSION['error'] = "Missing profile_id";
  header('location: index.php');
  return;
}
#Cancel button
if (isset($_POST['cancel'])) {
  header("Location: index.php"); #here it is not necessary to send 'profile_id'
  return;
}

//Load up profile information
$stmt = $pdo->prepare('SELECT * FROM profile WHERE profile_id = :prof AND user_id = :uid');
$stmt->execute(array(':prof' => $_REQUEST['profile_id'], ':uid' => $_SESSION['user_id']));
$profile = $stmt->fetch(PDO::FETCH_ASSOC);
//echo($_REQUEST['profile_id']);
if ($profile === false){
  $_SESSION['error'] = "Could not load profile";
  header('Location: index.php');
  return;
}
#Handling incomming data
if (isset($_POST['first_name']) && isset($_POST['last_name']) &&
    isset($_POST['email']) && isset($_POST['headline']) && isset
    ($_POST['summary'])) {

    $msg = validateProfile();
    if (is_string($msg)){
      $_SESSION['error'] = $msg;
      header("Location: edit.php?profile_id=" . $_REQUEST['profile_id']);
      return;
    }
    #validate position entries is there are.
    $msg = validatePos();
      if (is_string($msg)){
        $_SESSION['error'] = $msg;
        header("Location: edit.php?profile_id=" . $_REQUEST['profile_id']);
        return;
      }
    #validate Education entries
    $msg = validateEdu();
      if (is_string($msg)){
        $_SESSION['error'] = $msg;
        header("Location: edit.php?profile_id=" . $_REQUEST['profile_id']);
        return;
      }
        //Update profile
    $sql = "UPDATE profile SET first_name = :fn,
    last_name = :ln, email = :em, headline = :hl, summary = :su WHERE profile_id = :pid AND user_id = :uid";
    $stmt = $pdo->prepare($sql);
    echo ("<pre>\n".$sql."\n</pre>\n");
    $stmt->execute(array(
          ':pid' => $_REQUEST['profile_id'],
          ':uid' => $_SESSION['user_id'],
          ':fn' => $_POST['first_name'],
          ':ln' => $_POST['last_name'],
          ':em' => $_POST['email'],
          ':hl' => $_POST['headline'],
          ':su' => $_POST['summary']));

    #clear out the position entries
    $sql = "DELETE FROM position WHERE profile_id = :pid" ;
    $stmt = $pdo->prepare($sql);
    $stmt->execute(array(
      ':pid' => $_REQUEST['profile_id']));
    echo ("<pre>\n".$sql."\n</pre>\n");

    #Insert all current positions entries
    insertPositions($pdo, $_REQUEST['profile_id']);

    #clear out the Education entries
    $sql = "DELETE FROM education WHERE profile_id = :pid";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(array(
      ':pid' => $_REQUEST['profile_id']));
    echo ("<pre>\n".$sql."\n</pre>\n");

    #Insert all current positions entries
    insertEducations($pdo, $_REQUEST['profile_id']);

    echo ("<pre>\n".$positions."\n</pre>\n");
    $_SESSION['success'] = 'Profile updated';
    header("location: index.php");
    return;
}

//Load up the position
$positions = loadPos($pdo, $_REQUEST['profile_id']);
$schools = loadEdu($pdo, $_REQUEST['profile_id']);

print_r($positions);
print_r($schools);

/////////////////////////////////////HTML - VISUAL///////////////////////////////////
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- <script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script> -->

  <title>German Alfredo Chavarro Chavarro</title>
</head>

<body>
<div class="container">
<h1>Edit profile for <?php echo ($_SESSION['user_id'])?></h1>
<?php
flashMessages();
?>
<!--<fieldset>-->
  <form method="POST" action="edit.php">
    <input type="hidden" name="profile_id" value="<?= htmlentities($_GET['profile_id']); ?>"/>
    <p>First Name:<input type="text" name="first_name" value = "<?= htmlentities($profile['first_name']); ?>" size="80"></p>
    <p>Last Name:<input type="text" name="last_name" value = "<?= htmlentities($profile['last_name']); ?>" size="80"></p>
    <p>Email:<input type="text" name="email" value = "<?= htmlentities($profile['email']); ?>" size="40"></p>
    <p>Headline:<input type="text" name="headline" value = "<?=htmlentities($profile['headline']); ?>" size="40"></p>
    <p>Summary:<br/><textarea name="summary" rows="8" cols="80"><?=htmlentities($profile['summary']);?></textarea></p>
    <!--<div id='AddDel'>
    Position:<input type="button" id = "add" onclick="IncludePost(this); return false;" value="+">
  </div>-->
<?php
$pos = 0;
echo ('<p>Position: <input type="submit" id="addPos" value="+">'."\n");
echo ('<div id="position_fields">'."\n");
  foreach ($positions as $position) {
        $pos++;
        echo ('<div id="position'.$pos.'">'."\n");
        echo ('<p>Year: <input type="text" name="year'.$pos.'"');
        echo ('value="'.$position['year'].'"/>'."\n");
        echo ('<input type="button" value="-" onclick="$(\'#position'.$pos.'\').remove(); return false;">'."\n");
        echo ("</p>\n");
        echo ('<textarea name="desc'.$pos.'" rows="8" cols="80">'."\n");
        echo (htmlentities($position['description'])."\n");
        echo ("\n</textarea>\n</div>\n");
  }
echo ("</div></p>\n");

$countEdu = 0;
echo ('<p>Education: <input type="submit" id="addEdu" value="+">'."\n");
echo ('<div id="edu_fields">'."\n");
if ( count($schools) > 0){
  foreach ($schools as $school) {
      $countEdu++;
      echo ('<div id="edu'.$countEdu.'">');
      echo ('<p>Year: <input type="text" name="edu_year'.$countEdu.'"');
      echo ('value="'.$school['year'].'"/>'."\n");
      echo ('<input type="button" value="-" onclick="$(\'#edu'.$countEdu.'\').remove(); return false;">'."\n");
      echo ("</p>\n");
      echo ('<p>School: <input type="text" size="80" name="edu_school'.$countEdu.'" class="school" value="'.htmlentities($school['name']).'"/>'."\n");
      echo ("</div></p>\n");
    }
}
echo ("</div></p>\n");
?>

<p>
<input type="submit" value = "Save" size="40">
<input type="submit" name="cancel" value="Cancel">
</p>
</form>
<!--</fieldset>-->

<script>
countPos = <?= $pos ?>;
countEdu = <?= $countEdu ?>
//countPos = 0;
//https://stackoverflow.com/questions/17650776/add-remove-html-inside-div-using-javascript
$(document).ready(function(){
window.console && console.log("Document ready called");

$('#addPos').click(function(event){
    event.preventDefault();
    //http://api.jquery.com/event.preventdefault/
    //Description: If this method is called, the default action of the event will not be triggered.

    if (countPos >= 9){
      alert('Maximum of nine position entries exceeded');
      return;
    }
    countPos++;
    window.console && console.log("Addition Position" +countPos);
    $('#position_fields').append(
      '<div id="position'+countPos+'"> \
      <p>Year: <input type="text" name="year'+countPos+'" value="" />\
      <input type="button" value="-"\
      onclick = "$(\'#position'+countPos+'\').remove(); return false;"></p> \
      <textarea name="desc'+countPos+'" rows = "8" cols = "80"></textarea>\
      </div>');
    });

    $('#addEdu').click(function(event){
        event.preventDefault();
        //http://api.jquery.com/event.preventdefault/
        //Description: If this method is called, the default action of the event will not be triggered.

        if (countEdu >= 9){
          alert('Maximum of nine position entries exceeded');
          return;
        }
        countEdu++;
        window.console && console.log("Addition Education" +countEdu);

        //Grab some HTML with hot spots and insert into the Document
        var source = $("#edu-template").html();
        $('#edu_fields').append(source.replace(/@COUNT@/g,countEdu));

        //Add the event handler to the new position_fields
        $('.school').autocomplete({
          source: "school.php"
        });
  });
  $('.school').autocomplete({
    source: "school.php"
  });
});
</script>
<!--HTML with substitution hot spots -->
<script id='edu-template' type="text">
  <div id="edu@COUNT@">
  <p>Year: <input type"text" name="edu_year@COUNT@" value="" />
  <input type="button" value="-" onclick="$('#edu@COUNT@').remove();return false;"><br>
  <p>School: <input type="text" size="80" name="edu_school@COUNT@" class="school" value="" />
  </p>
  </div>
</script>
</div>
</body>
</html>
