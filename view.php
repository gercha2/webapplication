<?php
require_once "pdo.php";
require_once "util.php";
session_start();

    if ( ! isset($_SESSION['user_id']) ) {
      die("ACCESS DENIED");
    }
    if (!isset($_REQUEST['profile_id'])){
      $_SESSION['error'] = "Missing profile_id";
      header("location: index.php");
      return;
    }

$positions = loadPos($pdo, $_REQUEST['profile_id']);
$schools= loadEdu($pdo, $_REQUEST['profile_id']);

//print_r($positions);
//print_r($schools);

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name ="description" content="Database profiles, with login and redirect">
  <meta name = "viewport" content="width=device-width, initial-scale=1.0">
  <!--<link rel="stylesheet" href="style.css">-->
<title>German Alfredo Chavarro Chavarro</title>
</head>
<body style="font-family: sans-serif;">
  <h1>Profile Information <?php echo ($_SESSION['user_id'])?></h1>

<?
$stmt = $pdo->prepare('SELECT * FROM profile WHERE profile_id = :prof AND user_id = :uid');
$stmt->execute(array(':prof' => $_REQUEST['profile_id'], ':uid' => $_SESSION['user_id']));
$profile = $stmt->fetch(PDO::FETCH_ASSOC);
?>

<p>First Name: <?= $profile['first_name']; ?></p>
<p>Last Name: <?= $profile['last_name']; ?></p>
<p>Email: <?= $profile['email']; ?></p>
<p>Headline: <?= $profile['headline']; ?></p>
<p>Summary: <?= $profile['summary']; ?></p>

<p>Position:<br/>
<?php
//print_r($positions);
if($positions == false){
  $_SESSION['error'] = "No information found";
  return;
}
else{
foreach ($positions as $position){
  echo ("<ul>");
  echo ("<li>");
  echo (htmlentities($position['year'].": ".$position['description']));
  echo ("</li>");
  echo ("</ul>");
}}
?>
<p>Education:<br/>
<?php
if($schools == false){
  $_SESSION['error'] = "No information found";
  return;
}
else{
//print_r($schools);
//while ($row = $stmt->fetch()){
foreach ($schools as $row){
    echo ("<ul>");
    echo ("<li>");
    echo ($row['year'].': '.$row['name']);
    echo ("</li>");
    echo ("</ul>");
}
}
?>
<a href="index.php">Done</a>
</body>
</html>
