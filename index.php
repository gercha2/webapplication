<!--In this next assignment you will extend our simple resume database to support
Create, Read, Update, and Delete operations (CRUD) into a Position table that
 has a many-to-one relationship to our Profile table.

This assignment will use JQuery to dynamically add and delete positions in the
 add and edit user interface.

//Author: German Chavarro
//email: gercha2@gmail.com-->

<?php
require_once "pdo.php";
session_start();
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="description" content="database of automovile">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="style.css">
<title>German Alfredo Chavarro Chavarro</title>
</head>
<body><h1>German Chavarro's Resume Registry</h1>
<?php
if (isset($_SESSION['newsession']) == '1'){
    if ( isset($_SESSION['error']) ) {
        echo '<p style="color:red">'.$_SESSION['error']."</p>\n";
        unset($_SESSION['error']);
    }
    if ( isset($_SESSION['success']) ) {
        echo '<p style="color:green">'.$_SESSION['success']."</p>\n";
        unset($_SESSION['success']);
    }?>
<table border="1">
  <?php  //Show table with Database information
    $stmt = $pdo->query("SELECT first_name, last_name, headline, profile_id FROM profile");
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);?>
    <p><a href='add.php'>Add New Entry</a></p>
    <p><a href='logout.php'>Logout</a></p>
<?php
    if ($rows == false){
      echo ("No rows found");
    } else { ?>

    <?php
    echo "<tr><td>";
    echo ('<b>Name</b>');
    echo "</td><td>";
    echo ("<b>Headline</b>");
    echo "</td><td>";
    echo ("<b>Action</b>");
    foreach ($rows as $row) {
    echo "<tr><td>";
    echo ('<a href="view.php?profile_id='.$row['profile_id'].'">'.$row["first_name"]." ".$row["last_name"].'</a>');
    echo "</td><td>";
    echo (htmlentities($row["headline"]));
    echo "</td><td>";
    echo('<a href="edit.php?profile_id='.$row['profile_id'].'">Edit</a> / ');
    echo('<a href="delete.php?profile_id='.$row['profile_id'].'">Delete</a>');
    echo("</td></tr>\n");
    }
    }?>
    </table>

  <? } else {?>
    <p><a href='login.php'>Please log in</a></p>

    <table border="1">
      <?php  //Show table with Database information
        $stmt = $pdo->query("SELECT first_name, last_name, headline FROM profile");
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($rows == false){
          echo ("No rows found");
        } else {
        echo "<tr><td>";
        echo ('<b>Name</b>');
        echo "</td><td>";
        echo ("<b>Headline</b>");
        foreach ($rows as $row) {
        echo "<tr><td>";
        echo (htmlentities($row["first_name"]." ".$row["last_name"]));
        echo "</td><td>";
        echo (htmlentities($row["headline"]));
        echo("</td></tr>\n");
        }
        }?>
        </table>
        <p>Attempt to <a href='add.php'>add data </a>without logging in</p>

  <?php } ?>

</body>
</html>
