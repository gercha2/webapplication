<!--Autos with POST redirect

In this next assignment, you will expand a web based application to track data
about automobiles and store the data in a MySQL database.
All interactions will follow the POST-Redirect pattern where appropriate.
Note that there is no specific sample code for this assignment.

Redirect the browser to view.php-->

<?php
require_once "pdo.php";
$salt = 'XyZzy12*_';
//$passSavedHash='1a52e17fa899cf40fb04cfc42e6352f1';


session_start();
if ( isset($_POST['email']) && isset($_POST['pass'])) {
    //unset($_SESSION['account']);  // Logout current user
    $Result = strpos(htmlentities($_POST['email']),'@'); //False is not displayed.
    if (strlen(htmlentities($_POST['email'])) < 1 || strlen(htmlentities($_POST['pass'])) < 1) {
        $_SESSION["error"] = "User name and password are required";
        header("Location: login.php");
        return;
    } elseif ($Result === false){
      $_SESSION["error"] = 'Email must have an at-sign (@)';
      header("Location: login.php");
      return;
    } else {
      //$check = hash('md5', $salt.htmlentities($_POST['pass']));
      $check = hash('md5', $salt.$_POST['pass']);
      //User & password is validated into server
      $stmt = $pdo->prepare('SELECT user_id, name FROM users WHERE email = :em AND password = :pw');
      $stmt->execute(array( ':em' => $_POST['email'], ':pw' => $check));
      $row = $stmt->fetch(PDO::FETCH_ASSOC);
      if ($row !== false){
        $_SESSION['name'] = $row['name'];
        $_SESSION['user_id'] = $row['user_id'];
        $_SESSION['newsession'] = '1';
        error_log("Login success ".htmlentities($_POST['email']));
        header("location: index.php");
        return;
      } else {
          $_SESSION["error"] = "Incorrect password.";
          error_log("Login fail ".htmlentities($_POST['email'])." $check");
          header("Location: login.php");
          return;
        }
    }
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name ="description" content="Database for Autos, with login and redirect">
  <meta name = "viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="style.css">

<title>German Alfredo Chavarro Chavarro</title>
</head>
<body>
<h1>Please Log In</h1>
<script type="text/javascript">
function doValidate(){
  try {
  console.log("Javascript validation call");
  pw = document.getElementById('userPassword').value;
  console.log("Validating pw= "+pw);
  if (pw == null || pw == "") {
    //alert("Both fields must be filled out");
    alert('Invalid email address');
    return false;
  }
  return true;
} catch(e) {
    return false;
}
return false;
}
</script>

<?php
    if ( isset($_SESSION["error"]) ) {
        echo('<p style="color:red">'.$_SESSION["error"]."</p>\n");
        unset($_SESSION["error"]);
    }
?>
  <form method="POST">

  <p>User Name <input type="text" name="email" value=""><br/></p>
  <p>Password <input type="password" id="userPassword" name="pass" value="" minlength="6"><br/></p>

  <!--<input type="hidden" name="user_id" value="user_id">-->
  <!--<p><input type="submit" value="Log In">-->
  <!--<input type = "submit" name = "dopost">-->
  <input type="submit" onclick="return doValidate();" value="Log In">
  <input type="button" onclick="window.location.replace('index.php')" value="Cancel"></p>
  </form>
</body>
</html>
